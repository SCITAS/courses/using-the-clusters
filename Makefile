build/slides.pdf: slides.tex images
	mkdir -p output
	pdflatex -output-directory=output $<
	pdflatex -output-directory=output $<
	pdflatex -output-directory=output $<
	cp output/slides.pdf .

intermediary_images = images/hpc-cluster-multi-v2.pdf images/hpc-cluster-v2.pdf images/mc_support.png

images: $(intermediary_images)

%.pdf: %.svg
	inkscape --export-filename=$@ $^

%.png: %.gif
	convert $^ $@

.PHONY: clean, distclean

clean:
	rm -f *.aux *.log *.out $(intermediary_images)

distclean: clean
	rm -rf output
