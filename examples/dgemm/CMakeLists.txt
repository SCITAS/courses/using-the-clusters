project(dgemm)
cmake_minimum_required(VERSION 3.0)

find_package(BLAS)
find_path(BLAS_INCLUDE_DIR NAMES cblas.h mkl_cblas.h HINTS ${BLAS_LIBRARIES}/../include)
if(BLAS_mkl_core_LIBRARY)
  add_definitions(-DMKLINUSE)
endif()

add_executable(dgemm dgemm.c)
target_link_libraries(dgemm PRIVATE ${BLAS_LIBRARIES})
target_include_directories(dgemm PRIVATE ${BLAS_INCLUDE_DIR})
