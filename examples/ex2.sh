#!/bin/bash
#SBATCH --chdir /scratch/<username>/using-clusters/linpack
#SBATCH --nodes 1
#SBATCH --ntasks 1
#SBATCH --cpus-per-task 36
#SBATCH --mem 120G
#SBATCH --time 00:30:00
#SBATCH --account scitas-courses

./runme_xeon64
