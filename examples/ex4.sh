#!/bin/bash -l
#SBATCH --chdir /scratch/<put-your-username-here>/using-clusters
#SBATCH --nodes 2
#SBATCH --ntasks-per-node 36
#SBATCH --cpus-per-task 1
#SBATCH --mem 120G
#SBATCH --time 10:00
#SBATCH --qos parallel
#SBATCH --account scitas-courses

module purge
module load gcc
module load openmpi
module load osu-micro-benchmarks

srun osu_allgather
