\documentclass[usenames,dvipsnames]{beamer}
\usetheme{SCITAS}

\usepackage{graphicx}
\usepackage{fancyvrb}
\usepackage{verbatim}
\usepackage{latexsym}%for \leadsto
\usepackage{booktabs} % for nice tables
% for more space between rows
% \newcommand{\ra}[1]{\renewcommand{\arraystretch}{#1}}
\usepackage{multirow} % for nice tables

\title{Using the General Purpose Compute clusters at EPFL}
\author{SCITAS}
\date{May 22, 2024}
\institute{\url{https://go.epfl.ch/scitas-courses-clusters}}

\begin{document}

%\newcommand{\SubItem}[1]{
%    {\setlength\itemindent{15pt} \small\item[-] #1}
%}


\begin{frame}[noframenumbering]
  \titlepage
\end{frame}



\section{Welcome}

\begin{frame}
  \frametitle{Welcome}
  \begin{exampleblock}{What we will look into today}
    \begin{itemize}
      \item What is a cluster
      \item What is a scheduler
      \item Software: how is the environment organised
      \item Running simple jobs
    \end{itemize}
  \end{exampleblock}
\end{frame}



\section{Clusters}

\begin{frame}
  \frametitle{What is a cluster?}
  \includegraphics[width=\columnwidth]{images/hpc-cluster-v2}
\end{frame}


\begin{frame}
  \frametitle{What is a cluster?}
    \begin{exampleblock}{Distinctive characteristics}
      Configuration optimized for performance:
      \begin{itemize}
        \item Memory: maximum bandwidth
        \item Network: low latency and high bandwidth
        \item Storage: high performance parallel
        \item Accelerators: GPUs, Vectorization Processor
      \end{itemize}
    \end{exampleblock}
\end{frame}


\begin{frame}
  \frametitle{What is a cluster?}
  \includegraphics[width=\columnwidth]{images/hpc-cluster-multi-v2}
\end{frame}


\begin{frame}
  \frametitle{Our clusters}
  \begin{center}
    \includegraphics[width=\columnwidth]{images/cluster_timeline.2024.v2}
    \includegraphics[width=\columnwidth]{images/Helvetios-Izar-Jed}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{What are clusters optimised for?}
  \begin{block}{CPU workloads}
    \begin{itemize}
    \item Jobs running on \texttt{jed} (and \texttt{helvetios})
    \item Multi-node (or 'parallel') workloads
      %\SubItem {The whole node is allocated to a user, nodes are not shared in-between users.}
      \begin{itemize}
        \small \item {Typically, the whole node is allocated to a user}
        \small \item {\textbf{\emph{parallel}} QoS}
      \end{itemize}
    \item Single-node (or 'serial') workloads
%      \SubItem {Only the resources requested are allocated (cores and memory), jobs from other users can run on the remaining node resources.}
      \begin{itemize}
        \small \item {Jobs from other users can run on the same node}
        \small \item {\textbf{\emph{serial}} QoS}
      \end{itemize}
    \end{itemize}
  \end{block}
  \begin{block}{GPU workloads}
    \begin{itemize}
      \item Job runs mostly on the GPU
      \begin{itemize}
        \small  \item {Usually serial, but can be parallel}
        \small \item {clusters: \texttt{izar} (and soon \texttt{kuma})}
      \end{itemize}
    \end{itemize}
  \end{block}
\end{frame}



\section{Storage}

\begin{frame}
  \frametitle{Shared Storage (cluster)}
  \begin{block}{\texttt{/scratch}}
    \begin{itemize}
    \item designed for high performance:
      \begin{itemize}
        \item low redundancy
        \item no backups
      \end{itemize}
    \item local to each cluster
    \item automatic cleanup procedure deletes files without warning\\(older than thirty days, newer if occupancy reaches a certain threshold)
    \item \textbf{for disposable files: intermediary results, temporary files}
    \end{itemize}
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{Shared Storage (global)}
  \begin{block}{\texttt{/home}}
    \begin{itemize}
    \item per user quotas of 100GB
    \item backup in a different building
    \item daily snapshots (see \texttt{/home/.snapshots/})
    \item available on all clusters
    \item \textbf{for important files: source code, final results, theses}
    \end{itemize}
  \end{block}
\end{frame}

  
\begin{frame}
  \frametitle{Shared Storage (global)}
  \begin{block}{\texttt{/work}}
    \begin{itemize}
    \item per group/project (e.g. \texttt{/work/<lab>})
    \item 25 CHF per TB/year, paid monthly
    \item usage calculated daily
    \item can be backed up (price doubles)
    \item available on all clusters
    \item \textbf{for shared group files: software, datasets}
    \end{itemize}
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{Connecting to a cluster [Hands-on!]}
  \begin{exampleblock}{Connect using SSH}
    \vspace{0.3cm}
    \hspace{0.65cm} \texttt{ssh \textless username\textgreater@helvetios.epfl.ch}

    \hspace{0.65cm} \texttt{ssh -l \textless username\textgreater\hspace{0.20cm}helvetios.epfl.ch}
    %\vspace{-0.4cm}
    \begin{itemize}
      \item \textbf{Linux}: connect using \texttt{ssh}
      \item \textbf{MacOS}: connect using \texttt{ssh}
      \item \textbf{Windows}: PuTTY, git-bash, MobaXterm, WSL
    \end{itemize}
    \hspace{0.65cm} VPN may be needed (eduroam, at home, ...)
  \end{exampleblock}
  \begin{block}{Some basic shell commands}
    \begin{itemize}
      \item {\tt id}
      \item {\tt pwd}
      \item {\tt ls /scratch/\textless username\textgreater}
      \item {\tt cd /scratch/\textless username\textgreater}
    \end{itemize}
  \end{block}
\end{frame}



\section{Batch Systems}

\begin{frame}
  \frametitle{Batch versus Interactive use}
  \begin{block}{Interactive}
    You are in front of your computer, just open MATLAB and start working.
  \end{block}
  \begin{block}{Batch}
    You describe the work to be done (in a script) and put it in a queue. Your jobs will be run when matching resources become available.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{Batch system and Scheduler}
  \begin{block}{Batch system}
    Takes jobs and keeps them in a queue until the \textit{scheduler} decides they should be started, and then starts the jobs.
  \end{block}
  \begin{block}{Scheduler}
    Decides when and where your job will run depending on the requested resources and your priority. Its objective is to maximise the amount of work done with the resources available.
  \end{block}
  \center{We will sometimes use both terms interchangeably.}
\end{frame}


\begin{frame}
  \frametitle{SLURM}
  \includegraphics[width=\columnwidth]{images/SlurmLogo}
\end{frame}


\begin{frame}
  \frametitle{Slurm commands: \texttt{sbatch}}
  \begin{block}{\texttt{sbatch}}
    This is the fundamental command used to submit jobs to the batch system.
    Normally returns immediately, as all it does is read the script and check your requirements.
  \end{block}
  \begin{block}{Workflow}
    A typical workflow looks like this:
    \begin{itemize}
    \item create a job-script
    \item submit it to the batch system (with \texttt{sbatch})
    \item \textit{it will get executed}
    \item look at the output
    \end{itemize}
  \end{block}
  The job \textbf{will wait in the queue} until resources are available to run it.
\end{frame}


\begin{frame}
  \frametitle{Workflow of a job}
  \only<1>{
    \includegraphics[width=\columnwidth]{images/job-workflow-v2-0}
  }
  \only<2>{
    \includegraphics[width=\columnwidth]{images/job-workflow-v2-1}
  }
  \only<3>{
    \includegraphics[width=\columnwidth]{images/job-workflow-v2-2}
  }
  \only<4>{
    \includegraphics[width=\columnwidth]{images/job-workflow-v2-3}
  }
\end{frame}



\section{Exercise 1: sbatch}

\begin{frame}[fragile]
  \frametitle{Exercise 1: sbatch [Hands-on!]}
  \begin{exampleblock}{Copy the examples to your directory at /scratch}
\begin{verbatim}
    cp -r /scratch/examples/using-clusters .
    cd using-clusters
\end{verbatim}
  \end{exampleblock}
  \begin{exampleblock}{Open and edit the first exercise}
    Open the file ex1.sh with your editor of choice:
    \begin{itemize}
    \item {\tt nano}
    \item {\tt emacs}
    \item {\tt vim}
    \item \ldots
    \end{itemize}
  \end{exampleblock}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Exercise 1: sbatch [Hands-on!]}
  \begin{block}{ex1.sh}
    \lstinputlisting[style=myshell]{examples/ex1.sh}
  \end{block}
\end{frame}


\begin{frame}[fragile]
  \frametitle{SBATCH directives}
  \begin{block}{\texttt{\#SBATCH --something}}
    This is how you tell Slurm what resources the job needs.
  \end{block}
  \pause
  \begin{block}{Be careful with the syntax}
    \texttt{\# SBATCH --something}

    \texttt{\#\#SBATCH --something}

    \texttt{\#SBATCH something}

    \vspace{0.4cm}
    These are all wrong.
  \end{block}
\end{frame}


\begin{frame}[fragile]
  \frametitle{SBATCH: nodes}
  \begin{block}{The number of nodes per job}
    Examples:
    \begin{itemize}
      \item[] \texttt{--nodes 1}
      \item[] \texttt{--nodes 32}
    \end{itemize}
    If not specified the default is 1.
  \end{block}
\end{frame}


\begin{frame}[fragile]
  \frametitle{SBATCH: ntasks}
  \begin{block}{The number of MPI tasks per job}
    Examples:
    \begin{itemize}
      \item[] \texttt{--ntasks 1}
      \item[] \texttt{--ntasks 288}
    \end{itemize}
    If not specified the default is 1.
  \end{block}
  \pause
  \begin{block}{The number of MPI tasks \emph{per node}}
    Another option to define the same:
    \begin{itemize}
      \item[] \texttt{--ntasks-per-node=1}
      \item[] \texttt{--ntasks-per-node=36} (together with \texttt{--nodes=8})
    \end{itemize}
    If not specified the default is 1.
  \end{block}
\end{frame}


\begin{frame}[fragile]
  \frametitle{SBATCH: cpus-per-task}
  \begin{block}{The number of cores per task for multithreaded applications}
    Examples:
    \begin{itemize}
      \item[] \texttt{--cpus-per-task 1}
      \item[] \texttt{--cpus-per-task 36}
    \end{itemize}
    If not specified the default is 1.

    Cannot be more than the number of cores/cpus in a compute node!
  \end{block}
\end{frame}


%% \begin{frame}
%%   \frametitle{Socket, Core, Threads vs \textit{CPU}}
%%   \begin{center}
%%     \includegraphics[width=0.7\textwidth]{images/mc_support}
%%   \end{center}
%% \end{frame}


\begin{frame}[fragile]
  \frametitle{SBATCH: mem}
  \begin{block}{The required memory per node}
    Examples:
    \begin{itemize}
      \item[] \texttt{--mem 4096M}
      \item[] \texttt{--mem 120G}
    \end{itemize}
    If not specified the default is 4096MB per core on \texttt{izar} and 7000MB/core on \texttt{jed}.
  \end{block}
  \begin{block}{Don't be greedy}
    Don't ask for the exact amount of RAM on a node. The OS also needs some.
  \end{block}
\end{frame}


\begin{frame}[fragile]
  \frametitle{SBATCH: time}
  \begin{block}{How long will your job run for?}
    Examples:
    \begin{itemize}
      \item[] \texttt{--time 06:00:00}
      \item[] \texttt{--time 1-20:00:00}
      \item[] \texttt{--time \hspace{5.75pt} 44:00:00}
    \end{itemize}
    GPU/serial jobs can run for up to 3 days of wall time.

    Parallel jobs have a limit of 15 days.
  \end{block}
  \begin{block}{Choose the time wisely}
    Small jobs fit in the gaps between big jobs.
  \end{block}
\end{frame}


\begin{frame}[fragile]
  \frametitle{SBATCH: qos}
  \begin{block}{Which qos should my job be sent to?}
    Examples:
    \begin{itemize}
      \item[] \texttt{--qos debug}
      \item[] \texttt{--qos serial}
      \item[] \texttt{--qos parallel}
    \end{itemize}
    If not specified the default is \texttt{serial} on Helvetios/Jed. On Izar, the default is \texttt{gpu}.
  \end{block}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Exercise 1: submitting a job [Hands-on!]}
  \begin{exampleblock}{Let's submit our job}
    \begin{lstlisting}[style=myshell,gobble=6]
      $ sbatch ex1.sh
      Submitted batch job 12345678

      $ cat /scratch/<username>/slurm-12345678.out
      hello from h103
    \end{lstlisting}
  \end{exampleblock}
  \begin{block}{Remember the Job ID}
    The number returned by \texttt{sbatch} is known as the \textbf{Job ID} and is the unique identifier for a task. If you ever need to ask for help
    you’ll need to know this number.
  \end{block}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Cancelling jobs}
  \begin{block}{scancel}
    To cancel a specific job:
    \begin{lstlisting}[style=myshell,gobble=6]
      scancel <jobid>
    \end{lstlisting}
    To cancel all your jobs:
    \begin{lstlisting}[style=myshell,gobble=6]
      scancel -u <username>
    \end{lstlisting}
    To cancel all your jobs that are not yet running:
    \begin{lstlisting}[style=myshell,gobble=6]
      scancel -u <username> -t PENDING
    \end{lstlisting}
  \end{block}
\end{frame}


\section{Exercise 2: squeue}
\begin{frame}[fragile]
  \frametitle{Exercise 2: squeue [Hands-on!]}
  \begin{block}{120GB of memory required}
    \small
    \lstinputlisting[style=myshell]{examples/ex2.sh}
  \end{block}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Exercise 2: what's going on? [Hands-on!]}
  \begin{block}{\texttt{squeue}}
    With no arguments squeue will list all jobs currently in the queue! The output and information shown can be refined somewhat by giving options.
    \begin{itemize}
    \item \code{squeue -t R -u username}
    \item \code{squeue -t PD -u username}
    \item \code{squeue -t PD -u username --start}
    \end{itemize}
  \end{block}
  \begin{block}{\texttt{Squeue}}
    \texttt{Squeue} is a custom \texttt{squeue} that shows only your jobs with more useful information.
  \end{block}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Cancelling jobs [Hands-on!]}
  \begin{block}{\texttt{scancel}}
    \begin{itemize}
      \item Try and cancel your pending jobs.
      \item Try and cancel the jobs of the person to your left.
    \end{itemize}
  \end{block}
\end{frame}



\section{Exercise 3: modules}

\begin{frame}
  \frametitle{Software}
  \begin{block}{What's the problem?}
    \begin{itemize}
    \item The OS version is restricted to an older one due to compatibility
      requirements of storage systems and specialized interconnects.
    \item The above is often in direct conflict with the needs of our user
      community, for which newer versions bring performance improvements and
      support for newer hardware (new CPU features).
    \item Many scientific codes are not even packaged under most Linux
      distributions.
    \end{itemize}
  \end{block}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Modules to the rescue}
  \begin{block}{modules}
    \texttt{(GNU) \textbf{module}} is a utility that allows multiple, often incompatible, tools and libraries to co-exist on a system. It's a widely used tool for organising software on HPC clusters but each site uses it in subtly different ways.
  \end{block}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Modules}
  \begin{block}{How are software modules organised?}
    \begin{itemize}
    \item packages are organized hierarchically: \texttt{Compiler / MPI}
    \item packages are hidden until one loads the dependencies
    \item \textbf{modules} maintain the environment consistent
    \item \textbf{modules} will automatically reload any software when one of the hierarchy layers is changed
    \end{itemize}
  \end{block}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Exercise 3: modules [Hands-on!]}
  \begin{exampleblock}{Basic commands}
    \begin{itemize}
    \item {\tt module av(ailable)}
    \item {\tt module load / unload <module-name>}
    \item {\tt module spider <name>}
    \item {\tt module purge}
    \end{itemize}
  \end{exampleblock}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Exercise 3: modules [Hands-on!]}
  \begin{exampleblock}{Example: loading \texttt{python}}
    \begin{itemize}
    \item {\tt module load intel}
    \item {\tt module load python}
    \item {\tt module list}
    \item {\tt module load python/2.7.18}
    \item {\tt module load gcc}
    \item {\tt module list}
    \end{itemize}
  \end{exampleblock}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Exercise 3: modules [Hands-on!]}
  \begin{block}{ex3.sh: using module files}
    \scriptsize
    \lstinputlisting[style=myshell]{examples/ex3.sh}
  \end{block}
\end{frame}


%\begin{frame}[fragile]
%  \frametitle{Exercise 4: MPI parallel jobs [Hands-on!]}
%
% \begin{block}{\texttt{ex4.sh}: \texttt{srun} to run parallel jobs}
%    \begin{lstlisting}[style=myshell,gobble=6]
%     #!/bin/bash
%
%      #SBATCH --nodes 2
%      #SBATCH --ntasks 32
%      #SBATCH --cpus-per-task 1
%
%      module purge
%      module load intel
%      module load intel-mpi
%
%      cd osu
%      make clean
%     make
%
%      srun ./osu_allgather
%    \end{lstlisting}
%  \end{block}
% \end{frame}


\section{Exercise 4: Sinteract}
\begin{frame}[fragile]
  \frametitle{Interactive access}
  \begin{block}{Why interactive?}
    For debugging or running applications such as Matlab interactively we don’t want to submit a batch job.
  \end{block}
  \begin{block}{Sinteract or salloc}
    There are two main ways of getting access depending on what you want to achieve:
    \begin{itemize}
    \item {\tt salloc} - standard tool for an interactive allocation for multi-node jobs
    \item {\tt Sinteract} - custom tool to access a node
    \end{itemize}
    Behind the scenes both use the same mechanism as \texttt{sbatch} to get access to resources.

    (Check {\tt Sinteract -h} for all the options.)
  \end{block}
\end{frame}


%% \begin{frame}[fragile]
%%   \frametitle{Sinteract}
%%
%%   \begin{block}{\texttt{Sinteract --help}}
%% \begin{Verbatim}[fontsize=\footnotesize]
%% usage: Sinteract [-c cores] [-n tasks] [-t time] [-m memory]
%% [-p partition] [-a account] [-q qos] [-g resource] [-r reservation]
%% options:
%%   -c cores       cores per task (default: 1)
%%   -n tasks       number of tasks (default: 1)
%%   -t time        as hh:mm:ss (default: 00:30:00)
%%   -m memory      as #[K|M|G] (default: 4G)
%%   -p partition   (default: parallel)
%%   -a account     (default: scitas-ge)
%%   -q qos         as [normal|gpu|gpu_free|mic|...] (default: )
%%   -g resource    as [gpu|mic][:count] (default is empty)
%%   -r reservation reservation name (default is empty)
%%   -s contraints  list of required features (default is empty)
%%                      Deneb/Eltanin: E5v2, E5v3
%%                      Fidis/Gacrux: E5v4, s6g1, mem128gb, mem192gb, mem256gb
%% examples:
%%     /usr/bin/Sinteract -c 16 -p serial
%%     /usr/bin/Sinteract -p gpu -q gpu_free -g gpu:1
%% \end{Verbatim}
%%   \end{block}
%% \end{frame}


\begin{frame}[fragile]
  \frametitle{\texttt{Sinteract} [Hands-on!]}
  \begin{exampleblock}{Sinteract}
\begin{Verbatim}[fontsize=\footnotesize]
[<user>@helvetios ~]$ Sinteract -q debug
Cores:            1
Tasks:            1
Time:             00:30:00
Memory:           4G
Partition:        standard
Account:          scitas-courses
Jobname:          interact
Resource:
QOS:              debug
Reservation:

salloc: Granted job allocation 12345678
salloc: Waiting for resource configuration
salloc: Nodes h106 are ready for job
[<user>@h106 ~]$
\end{Verbatim}
  \end{exampleblock}
\end{frame}


%\begin{frame}[fragile]
%  \frametitle{salloc [Hands-on!]}
%
%  \begin{exampleblock}{salloc then srun for MPI tasks}
%\begin{Verbatim}[fontsize=\small]
%[<user>@deneb2 ]$ salloc -N 1 -n 2 --mem 2048 -p debug
%salloc: Granted job allocation 1451788
%salloc: Waiting for resource configuration
%salloc: Nodes r02-node02 are ready for job
%
%[<user>@deneb2 ]$ hostname
%deneb2
%
%[<user>@deneb2 ]$ srun hostname
%r02-node02
%r02-node02
%
%[<user>@deneb2 ]$ exit
%salloc: Relinquishing job allocation 1451788
%\end{Verbatim}
%  \end{exampleblock}
%\end{frame}

\begin{frame}[fragile]
  \frametitle{The S tools}
  \begin{block}{}
    Wrappers around Slurm commands to make your life (slightly) easier:
    \begin{itemize}
      \item {\tt Sinteract} - get interactive access to a node
      \item {\tt Sshare} - show fairshare information
      \item {\tt Squeue} - show user's pending and running jobs
      \item {\tt Sjob} - show information about a job
      \item {\tt sausage} - shows estimated cost of all jobs
    \end{itemize}
  \end{block}
\end{frame}


\begin{frame}[fragile]
  \frametitle{\texttt{Sinteract}: storage [Hands-on!]}
  \begin{block}{Storage locations}
    The different storage locations are accessible via environment variables.
    \begin{itemize}
    \item \texttt{\$TMPDIR} - a \textbf{temporary} folder in a \textbf{local} filesystem (generally \texttt{/tmp})
    \item \texttt{\$SCRATCH} - your scratch folder at  \texttt{/scratch/\textless username\textgreater}
    \end{itemize}
  \end{block}
 \begin{exampleblock}{Try it: \texttt{Sinteract -p debug}}
\begin{Verbatim}[fontsize=\small]

[<user>@h107 ~]$ echo $TMPDIR
/tmp/123456789

\end{Verbatim}
  \end{exampleblock}
  \begin{center}
    Note that these are only set within a job!
  \end{center}
\end{frame}



\section{Debugging}

\begin{frame}[fragile]
  \frametitle{Debugging}
  \begin{block}{\texttt{--qos=debug}}
    A high priority QoS allows short jobs, intended to quickly debug jobs:
    \begin{itemize}
      \item {\tt \#SBATCH -q debug}
      \item {\tt Sinteract -q debug}
    \end{itemize}
    On \texttt{izar} a node is reserved for this effect. Use:
    \begin{itemize}
      \item {\tt Sinteract -q debug -p debug}
      \item {\tt \#SBATCH -q debug

                 \#SBATCH -p debug}
    \end{itemize}
  \end{block}
\end{frame}


\section{Build Partition}
\begin{frame}[fragile]
  \frametitle{The build partition}
  \begin{block}{\texttt{--partition=build}}
    On \texttt{izar} you may need a GPU to compile your software.
    \begin{itemize}
    \item {\tt Sinteract -p build -c <n>}
    \end{itemize}
    Highly recommended: using the login nodes will be slower when {\tt <n>}  is a number of cores greater than 1.

    You may also have issues with RAM on the login node.
  \end{block}
  \begin{block}{The other clusters}
    On the other clusters you can use the \texttt{debug} QoS for this purpose as well.
  \end{block}
\end{frame}


\section{Fairshare}
\begin{frame}[fragile]
  \frametitle{Fairshare}
  \begin{block}{Not everyone is equal}
    All the groups have an equal priority within our clusters. All else being equal, older jobs have higher priority.

    \vspace{0.5cm}
    Within a group the relative consumption of the members decides who has more priority. Infrequent users go first.

    \vspace{0.5cm}
    \url{http://slurm.schedmd.com/fair\_tree.html}
    \url{http://slurm.schedmd.com/priority\_multifactor.html}
  \end{block}
\end{frame}


% Not generally given in the context of this training
% \include{compilation}


\section{Help!}
\begin{frame}
  \frametitle{Helping yourself}
  \begin{block}{{\tt man} pages are your friends!}
    \begin{itemize}
    \item {\tt man sbatch}
    \item {\tt man sacct}
    \item {\tt man gcc}
    \item {\tt module load intel; {\tt man ifort}}
    \end{itemize}
  \end{block}
\end{frame}



\section{Help!}

\begin{frame}
  \frametitle{Helping yourself}
  \begin{block}{get the examples!}
    \footnotesize
    git clone https://c4science.ch/diffusion/SCEXAMPLES/scitas-examples.git\\
    \normalsize
    %\vspace{1cm}
    %See the Modules directory for Comsol and Ansys examples.
  \end{block}
\end{frame}


\include{going_further}



\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
